import React, {Component}from 'react';
import './Pin.css'
import {connect} from "react-redux";
import actionTypes from '../../store/actionTypes'

class Pin extends Component{
    render(){
        let cls = 'disp';
        if(this.props.string === 'Access granted' ){
            cls = 'disp granted'
        }else if(this.props.string === 'Access denied'){
            cls = 'disp denied'
        }
            return(
           <div className="pin">
               <h2 className={cls}>{this.props.string}</h2>
               <div className="buttons">
                   <button onClick={() => this.props.number(1)} className="btn">1</button>
                   <button onClick={() => this.props.number(2)} className="btn">2</button>
                   <button onClick={() => this.props.number(3)} className="btn">3</button>
                   <button onClick={() => this.props.number(4)} className="btn">4</button>
                   <button onClick={() => this.props.number(5)} className="btn">5</button>
                   <button onClick={() => this.props.number(6)} className="btn">6</button>
                   <button onClick={() => this.props.number(7)} className="btn">7</button>
                   <button onClick={() => this.props.number(8)} className="btn">8</button>
                   <button onClick={() => this.props.number(9)} className="btn">9</button>
                   <button onClick={() => this.props.number(0)} className="btn">0</button>
                   <button onClick={this.props.back} className="btn">B</button>
                   <button onClick={this.props.enter} className="btn">E</button>
                   <button onClick={this.props.clear} className="btn">Clear</button>
               </div>
           </div>
        )
    }
}
const mapStateToProps = state =>{
    return{
        string: state.display
    }
};

const mapDispatchToProps = dispatch =>{
    return {
        number: (amount) => dispatch({type: actionTypes.number, amount: amount}),
        back: () => dispatch({type: actionTypes.back}),
        enter: () => dispatch({type: actionTypes.enter}),
        clear: () => dispatch({type: actionTypes.clear}),
    }


};

export default connect(mapStateToProps, mapDispatchToProps)(Pin);


