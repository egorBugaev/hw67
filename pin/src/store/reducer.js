import actionTypes from './actionTypes'
const initialState = {
       string: '',
       display: ''
};

const reducer = (state = initialState, action) => {


    switch (action.type){
        case actionTypes.number:
            return {string: state.string + action.amount, display: state.display + '*'};
        case actionTypes.back:
            return {string: state.string.substring(0, state.string.length - 1),
                    display: state.display.substring(0, state.display.length - 1) };
        case actionTypes.enter:
            if (parseInt(state.string, 10) === actionTypes.pin){
                return{display: 'Access granted'}
            }else{
                return{display: 'Access denied'};
            }
        case actionTypes.clear:
            return {string: '', display:''};

        default :
            return state
    }

};

export default reducer;
