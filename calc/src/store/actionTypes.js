export default {
    number:'number',
    minus:'minus',
    plus:'plus',
    dot:'dot',
    is:'is',
    divide:'divide',
    multiply:'multiply',
    clear:'clear'
}