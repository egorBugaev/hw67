import actionTypes from './actionTypes'
const initialState = {
       string: ''
};

const reducer = (state = initialState, action) => {
    if(!state.string){
        state.string=''
    }
    switch (action.type){
        case actionTypes.number:
            return {string: state.string + action.amount};
        case actionTypes.is:
            return {string: eval(state.string)};
        case actionTypes.dot:
            return {string: state.string + action.amount};
        case actionTypes.divide:
            return {string: state.string + action.amount};
        case actionTypes.minus:
            return {string: state.string + action.amount};
          case actionTypes.plus:
              return {string: state.string + action.amount};
        case actionTypes.multiply:
            return {string: state.string + action.amount};
         case actionTypes.clear:
            return {string: ''};
        default :
            return state
    }

};

export default reducer;
