import React, {Component}from 'react';
import './Calc.css'
import {connect} from "react-redux";
import actionTypes from '../../store/actionTypes'

class Calc extends Component{

    render(){
        return(
           <div className="calc">
               <h2 className="disp">{this.props.string}</h2>
               <div className="buttons">
                   <button onClick={() => this.props.number(1)} className="btn">1</button>
                   <button onClick={() => this.props.number(2)} className="btn">2</button>
                   <button onClick={() => this.props.number(3)} className="btn">3</button>
                   <button onClick={() => this.props.number(4)} className="btn">4</button>
                   <button onClick={() => this.props.number(5)} className="btn">5</button>
                   <button onClick={() => this.props.number(6)} className="btn">6</button>
                   <button onClick={() => this.props.number(7)} className="btn">7</button>
                   <button onClick={() => this.props.number(8)} className="btn">8</button>
                   <button onClick={() => this.props.number(9)} className="btn">9</button>
                   <button onClick={() => this.props.number(0)} className="btn">0</button>
                   <button onClick={this.props.divide} className="btn">/</button>
                   <button onClick={this.props.multiple} className="btn">*</button>
                   <button onClick={this.props.plus} className="btn">+</button>
                   <button onClick={this.props.minus} className="btn">-</button>
                   <button onClick={this.props.dot} className="btn">,</button>
                   <button onClick={this.props.is} className="btn">=</button>
                   <button onClick={this.props.clear} className="btn">Clear</button>
               </div>
           </div>
        )
    }
}
const mapStateToProps = state =>{
    return{
        string: state.string
    }
};

const mapDispatchToProps = dispatch =>{
    return {
        number: (amount) => dispatch({type: actionTypes.number, amount: amount}),
        dot: () => dispatch({type: actionTypes.dot, amount: '.'}),
        minus: () => dispatch({type: actionTypes.minus, amount: '-'}),
        plus: () => dispatch({type: actionTypes.plus, amount: '+'}),
        divide: () => dispatch({type: actionTypes.divide, amount: '/'}),
        multiple: () => dispatch({type: actionTypes.multiply, amount: '*'}),
        is: () => dispatch({type: actionTypes.is}),
        clear: () => dispatch({type: actionTypes.clear}),
    }


};

export default connect(mapStateToProps, mapDispatchToProps)(Calc);


